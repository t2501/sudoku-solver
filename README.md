# Sudoku Solver

Sudoku Solver is a computational tool designed to provide efficient solutions for the classic Sudoku puzzle. Developed in two distinct versions - C (command-line interface) and Excel/VB (with a user interface) - it caters to different user preferences and environments.

![Sudoku Image](URL_to_an_image_of_your_solver_or_a_sudoku_puzzle)

## Table of Contents

- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Contribution](#contribution)
- [License](#license)

## Features

1. **Versatility:** Available in both command-line (C) and GUI (Excel/VB) versions.
2. **Efficient Logic:** Uses constraint propagation and potential backtracking for accurate solutions.
3. **File Interactions (C Version):** Load and save Sudoku puzzles using simple file handling.
4. **User-Friendly UI (Excel/VB Version):** Input puzzles and view solutions with ease.

## Installation

### C Version

1. Clone the repository: `git clone <repository_URL>`
2. Navigate to the project directory: `cd path/to/sudoku-solver-c-version`
3. Compile the C code: `gcc main.c -o sudoku_solver`

### Excel/VB Version

1. Download the Excel file from the repository.
2. Ensure macros are enabled when opening the file in Microsoft Excel.

## Usage

### C Version

1. Run the compiled executable: `./sudoku_solver`
2. Follow the on-screen prompts to input your Sudoku puzzle or load from a file.

### Excel/VB Version

1. Open the Excel file.
2. Input your Sudoku puzzle in the designated cells.
3. Click the "Solve" button to view the solution.

## Contribution

Feedback and contributions are always welcome. Please create an issue or submit a pull request if you have suggestions or improvements to offer.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

